//
//  NMAppDelegate.m
//  NMUITests
//
//  Created by Alex Drozhak on 12/17/12.
//  Copyright (c) 2012 Alex Drozhak. All rights reserved.
//

#import "NMAppDelegate.h"

@implementation NMAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
  // Override point for customization after application launch.

  NMPillController *rootPillController =
  [[NMPillController alloc] init];

  NMPillNavigationController *pillNC =
  [[NMPillNavigationController alloc] initWithRoot:rootPillController];

  self.window.rootViewController = pillNC;
  self.window.backgroundColor = [UIColor whiteColor];
  [self.window makeKeyAndVisible];
  return YES;
}

@end
