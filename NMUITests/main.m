//
//  main.m
//  NMUITests
//
//  Created by Alex Drozhak on 12/17/12.
//  Copyright (c) 2012 Alex Drozhak. All rights reserved.
//

#import "NMAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([NMAppDelegate class]));
  }
}
