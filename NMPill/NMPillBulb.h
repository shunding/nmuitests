//
// NMPillBulb.h
// NMPillController
// 
// Created by Alex Drozhak on 12/17/12.
//
// Copyright (c) 2012 OLEArt.net. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>

typedef enum {
  kNMPillBulbAlignmentLeft,
  kNMPillBulbAlignmentRight,
  kNMPillBulbAlignmentBottom
} NMPillBulbAlignment;

@protocol NMPillBulbDelegate;

@class NMPillView;

@interface NMPillBulb : UIView

@property (nonatomic, assign) id<NMPillBulbDelegate> delegate;

@property (nonatomic, strong) UIColor *color;
@property (nonatomic, strong) NSString *title;

@property (nonatomic) NMPillBulbAlignment alignment;

@property (nonatomic, strong) NMPillView *parentView;

@property (nonatomic) BOOL bookmarked;

@property (nonatomic) CGFloat diameter;
@property (nonatomic) CGPoint originalCenter;

/*!
 * <#Method Overview#>
 * @param <#Parameter Name#> <#Parameter Description#>
 * @result <#Returned Result Description#>
 */
- (void)startMovement;

/*!
 * <#Method Overview#>
 * @param <#Parameter Name#> <#Parameter Description#>
 * @result <#Returned Result Description#>
 */
- (void)stopMovement;

@end

@protocol NMPillBulbDelegate <NSObject>

- (void)pillViewBulb:(NMPillBulb *)bulb movedToNewOrigin:(CGPoint)newOrigin;

@end