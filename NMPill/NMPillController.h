//
//  NMPillController.h
//  NMUITests
//
//  Created by Alex Drozhak on 12/17/12.
//  Copyright (c) 2012 Alex Drozhak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>
#import "NMPillView.h"

@class NMPillNavigationController;

/*!
 * This class is a main class for MNViewController
 */
@interface NMPillController : UIViewController <NMPillDelegate, NMPillDataSource>

@property (nonatomic, strong) NMPillView *pillView;

@property (nonatomic, strong) NMPillNavigationController *pillNavigationController;

@end
