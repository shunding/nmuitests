//
//  NMPillNavigationBorder.m
//  NMUITests
//
//  Created by Alex Drozhak on 12/20/12.
//  Copyright (c) 2012 Alex Drozhak. All rights reserved.
//

#import "NMPillNavigationBorder.h"

@interface NMPillNavigationBorder () {
  NMPillNavigationBorderType _borderType;
  BOOL _hidden;
}

@end

@implementation NMPillNavigationBorder

- (void)setBorderColor:(UIColor *)borderColor {
  _borderColor = borderColor;
  [self setNeedsDisplay];
}

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    self.backgroundColor = [UIColor clearColor];
  }
  return self;
}

- (id)initWithFrame:(CGRect)frame andType:(NMPillNavigationBorderType)borderType {
  self = [self initWithFrame:frame];
  if (self) {
    _borderType = borderType;
  }
  return self;
}

- (void)hideNavigationBorderAnimated:(BOOL)animated {
  if (_hidden) {
    return;
  }
  if (animated) {
    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(void) {
                       switch (_borderType) {
                         case kNMPillNavigationBorderTypeLeft:
                           self.transform =
                           CGAffineTransformMakeTranslation(-self.frame.size.width, 0.0f);
                           break;

                         case kNMPillNavigationBorderTypeTop:
                           self.transform =
                           CGAffineTransformMakeTranslation(0.0f, -self.frame.size.height);
                           break;

                         case kNMPillNavigationBorderTypeRight:
                           self.transform =
                           CGAffineTransformMakeTranslation(self.frame.size.width, 0.0f);
                           break;

                         case kNMPillNavigationBorderTypeBottom:
                           self.transform =
                           CGAffineTransformMakeTranslation(0.0f, self.frame.size.height);

                         default:
                           break;
                       }
                     }
                     completion:nil];
  } else {
    switch (_borderType) {
      case kNMPillNavigationBorderTypeLeft:
        self.transform =
        CGAffineTransformMakeTranslation(-self.frame.size.width, 0.0f);
        break;

      case kNMPillNavigationBorderTypeTop:
        self.transform =
        CGAffineTransformMakeTranslation(0.0f, -self.frame.size.height);
        break;

      case kNMPillNavigationBorderTypeRight:
        self.transform =
        CGAffineTransformMakeTranslation(self.frame.size.width, 0.0f);
        break;

      case kNMPillNavigationBorderTypeBottom:
        self.transform =
        CGAffineTransformMakeTranslation(0.0f, self.frame.size.height);

      default:
        break;
    }
  }
  _hidden = YES;
}

- (void)showNavigationBorderAnimated:(BOOL)animated {
  if (!_hidden) {
    return;
  }
  if (animated) {
    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(void) {
                       self.transform = CGAffineTransformIdentity;
                     }
                     completion:nil];

  } else {
    self.transform = CGAffineTransformIdentity;
  }
  _hidden = NO;
}

- (void)drawRect:(CGRect)rect {
  [super drawRect:rect];
  UIBezierPath *path = [UIBezierPath bezierPathWithRect:rect];
  [_borderColor setFill];
  [path fill];
}

@end
