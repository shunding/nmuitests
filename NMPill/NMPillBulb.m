//
// NMPillBulb.m
// NMPillController
// 
// Created by Alex Drozhak on 12/17/12.
//
// Copyright (c) 2012 OLEArt.net. All rights reserved.
//

#import "NMPillBulb.h"
#import "NMPillView.h"

#define MARGIN 30.0f

@implementation NMPillBulb {
  CMMotionManager *_motionManager;
  BOOL _dragging;
  BOOL _leftBookmarked;
  BOOL _rightBookmarked;
  BOOL _allowRightTransition;
  BOOL _bottomBookmarked;
  BOOL _allowBottomTransition;
  CGPoint _oldPosition;
  CGPoint _originalPosition;
}

- (void)setAlignment:(NMPillBulbAlignment)alignment {
  [self stopMovement];
  _alignment = alignment;
  switch (alignment) {
    case kNMPillBulbAlignmentLeft:
      _leftBookmarked = YES;
      break;

    case kNMPillBulbAlignmentRight:
      _rightBookmarked = YES;
      break;

    case kNMPillBulbAlignmentBottom:
      _bottomBookmarked = YES;
      break;

    default:
      break;
  }
}

- (void)startMovement {
  [_motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue]
                                       withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
                                         CGFloat top = _originalPosition.y - MARGIN;
                                         CGFloat bottom = _originalPosition.y + MARGIN;
                                         CGFloat left = _originalPosition.x - MARGIN;
                                         CGFloat right = _originalPosition.x + MARGIN;

                                         CGFloat dx = (CGFloat)accelerometerData.acceleration.x;
                                         CGFloat dy = (CGFloat)accelerometerData.acceleration.y;

                                         CGPoint newCenter = self.center;
                                         newCenter.x += dx;
                                         newCenter.y -= dy;
                                         if (newCenter.x < left) newCenter.x = left;
                                         if (newCenter.x > right) newCenter.x = right;
                                         if (newCenter.y < top) newCenter.y = top;
                                         if (newCenter.y > bottom) newCenter.y = bottom;
                                         self.center = newCenter;
                                       }];
}

- (void)stopMovement {
  [_motionManager stopAccelerometerUpdates];
}

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    self.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tapGR =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(didSelect)];
    tapGR.numberOfTapsRequired = 1;

    // Add Long Tap Gesture Recognizer
    UILongPressGestureRecognizer *longPressGR =
    [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                  action:@selector(didLongPressed)];
    longPressGR.minimumPressDuration = 1.0f;
    self.gestureRecognizers = [NSArray arrayWithObjects:tapGR, longPressGR, nil];

    // Initialize motion manager
    _motionManager = [[CMMotionManager alloc] init];
    _motionManager.accelerometerUpdateInterval = 1.0f / 60.0f; // 60 fps
    [self startMovement];
  }
  return self;
}

- (void)didMoveToSuperview {
  _originalPosition = self.center;
}

- (void)setBookmarked:(BOOL)bookmarked {
  _bookmarked = bookmarked;
  [self setNeedsDisplay];
}

- (void)didSelect {
  [self.parentView.delegate didSelectPillViewBulb:self];
}

- (void)didLongPressed {
  if ([self.parentView.delegate respondsToSelector:@selector(didLongPressedPillViewBulb:)]) {
    [self.parentView.delegate didLongPressedPillViewBulb:self];
  }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
  [self stopMovement];
  UITouch *touch = event.allTouches.anyObject;
  CGPoint touchLocation = [touch locationInView:self];
  if (_rightBookmarked && self.frame.origin.x < self.parentView.frame.size.width) {
    _allowRightTransition = NO;
  } else {
    _allowRightTransition = YES;
  }
  if (_bottomBookmarked && self.frame.origin.y < self.parentView.frame.size.height) {
    _allowBottomTransition = NO;
  } else {
    _allowBottomTransition = YES;
  }

  if (_leftBookmarked || _rightBookmarked) {
    [self.parentView dismissPillMenuItemsForPillBulb:self animated:YES];
  }

  _dragging = YES;
  _oldPosition = touchLocation;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
  UITouch *touch = event.allTouches.anyObject;
  CGPoint touchLocation = [touch locationInView:self.parentView];
  if (_dragging) {
    CGRect frame = self.frame;
    frame.origin.x = touchLocation.x - _oldPosition.x;
    frame.origin.y = touchLocation.y - _oldPosition.y;
    self.frame = frame;
    if (self.frame.origin.x < self.parentView.frame.origin.x) {
      _leftBookmarked = YES;
      _dragging = NO;
      [self.parentView.delegate willTransitPillViewBulb:self
                                            atDirection:kNMTransitionDirectionLeft];
    } else if (_allowRightTransition && self.frame.origin.x + self.frame.size.width > self.parentView.frame.size.width) {
      _rightBookmarked = YES;
      _dragging = NO;
      [self.parentView.delegate willTransitPillViewBulb:self
                                            atDirection:kNMTransitionDirectionRight];
    } else if (_allowBottomTransition && self.frame.origin.y + self.frame.size.height > self.parentView.frame.size.height) {
      _bottomBookmarked = YES;
      _dragging = NO;
      [self.parentView.delegate willTransitPillViewBulb:self
                                            atDirection:kNMTransitionDirectionBottom];
    } else if (self.frame.origin.x > 0 ||
               self.frame.origin.x < self.parentView.frame.size.width ||
               self.frame.origin.y < self.parentView.frame.size.height) {
      _leftBookmarked = NO;
      _rightBookmarked = NO;
      _bottomBookmarked = NO;
      _bookmarked = NO;
      frame.origin.x = touchLocation.x - _diameter / 2.0f;
      frame.origin.y = touchLocation.y - _diameter / 2.0f;
      frame.size.width = _diameter;
      frame.size.height = _diameter;
      self.frame = frame;
    }
  }
  [self setNeedsDisplay];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
  _dragging = NO;
  if (!_bookmarked) {
    [self.parentView rearrangePillsOriginalPositions];
    [self startMovement];
  }
  [self setNeedsDisplay];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
  _dragging = NO;
  if (!_bookmarked) {
    [self.parentView rearrangePillsOriginalPositions];
    [self startMovement];
  }
  [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
  [super drawRect:rect];
  CGContextRef context = UIGraphicsGetCurrentContext();
  CGContextSetFillColorWithColor(context, self.color.CGColor);
  if (!_bookmarked) {
    CGPoint center = CGPointMake(self.frame.size.width / 2.0f, self.frame.size.height / 2.0f);

    CGContextAddArc(context,
                    center.x,
                    center.y,
                    self.frame.size.width / 2.0f,
                    0.0f,
                    (CGFloat)( M_PI * 2.0f ), YES);
    CGContextFillPath(context);

    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    // Draw title
    CGRect titleFrame = CGRectMake(rect.origin.x + 10.0f,
                                   rect.size.height / 2.0f - 10.0f,
                                   rect.size.width - 20.0f,
                                   20.0f);
    [self.title drawInRect:titleFrame
                  withFont:[UIFont boldSystemFontOfSize:16.0f]
             lineBreakMode:NSLineBreakByTruncatingMiddle
                 alignment:NSTextAlignmentCenter];
  } else {
    CGContextFillRect(context, rect);

    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);

    // Draw title
    CGRect titleFrame = CGRectMake(rect.origin.x + 2.0f,
                                   rect.origin.y,
                                   rect.size.width - 4.0f,
                                   rect.size.height);

    NSString *uppercaseTitle = [_title uppercaseString];
    [uppercaseTitle drawInRect:titleFrame
                      withFont:[UIFont boldSystemFontOfSize:14.0f]
                 lineBreakMode:NSLineBreakByCharWrapping
                     alignment:NSTextAlignmentCenter];
  }
}

@end