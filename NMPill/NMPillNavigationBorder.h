//
//  NMPillNavigationBorder.h
//  NMUITests
//
//  Created by Alex Drozhak on 12/20/12.
//  Copyright (c) 2012 Alex Drozhak. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
  kNMPillNavigationBorderTypeLeft,
  kNMPillNavigationBorderTypeTop,
  kNMPillNavigationBorderTypeRight,
  kNMPillNavigationBorderTypeBottom
} NMPillNavigationBorderType;

@class NMPillBulb;

@interface NMPillNavigationBorder : UIView

@property (nonatomic, strong) UIColor *borderColor;

- (id)initWithFrame:(CGRect)frame andType:(NMPillNavigationBorderType)borderType;

- (void)hideNavigationBorderAnimated:(BOOL)animated;
- (void)showNavigationBorderAnimated:(BOOL)animated;

@end
