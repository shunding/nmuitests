//
//  NMPillView.m
//  NMUITests
//
//  Created by Alex Drozhak on 12/17/12.
//  Copyright (c) 2012 Alex Drozhak. All rights reserved.
//

#import "NMPillView.h"
#import "NMPillMenuItem.h"

#define MAIN_BULB_DIAMETER 140.0f

@implementation NMPillView {
  NMPillBulb *_mainBulb;

  NSMutableArray *_bulbs;
  NSMutableArray *_pillMenuItems;
}

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    self.backgroundColor = [UIColor clearColor];
    _bulbs = [NSMutableArray array];
    _pillMenuItems = [NSMutableArray array];
  }
  return self;
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
  [super willMoveToSuperview:newSuperview];
  // Get main bulb
  _mainBulb = [_dataSource mainBulbForPillView];
  _mainBulb.delegate = self;
  _mainBulb.parentView = self;
  // Draw main bulb centered in the main view with diameter 115 pt
  CGRect mainBulbFrame = CGRectMake(self.frame.size.width / 2.0f -
                                    MAIN_BULB_DIAMETER / 2.0f,
                                    self.frame.size.height / 2.0f -
                                    MAIN_BULB_DIAMETER / 2.0f,
                                    MAIN_BULB_DIAMETER,
                                    MAIN_BULB_DIAMETER);
  _mainBulb.frame = mainBulbFrame;
  _mainBulb.diameter = MAIN_BULB_DIAMETER;
  _mainBulb.originalCenter = _mainBulb.center;
  [_bulbs addObject:_mainBulb];
  [self addSubview:_mainBulb];

  CGFloat originY = self.frame.origin.y + 4.0f;
  for (NSString *menuTitle in [_dataSource pillMenuItemsForPillBulb:_mainBulb]) {
    NMPillMenuItem *anItem = [[NMPillMenuItem alloc] initWithFrame:CGRectZero];
    anItem.frame = CGRectMake(self.frame.origin.x - self.frame.size.width,
                              originY,
                              self.frame.size.width - 20.0f,
                              50.0f);
    anItem.title = menuTitle;
    anItem.color = [UIColor redColor];
    [_pillMenuItems addObject:anItem];
    [self insertSubview:anItem atIndex:0];
    originY += anItem.frame.size.height + 4.0f;
  }

  // Get all other pill bulbs
  if ([_dataSource numberOfBulbsInPillView:self] > 0) {
    for (NSUInteger i = 0; i < [_dataSource numberOfBulbsInPillView:self]; i++) {
      // Get the pill bulb instance
      NMPillBulb *bulb = [_dataSource bulbForPillViewAtIndex:i];
      bulb.diameter = [_dataSource diameterForBulbAtIndex:i];
      bulb.color = [_dataSource colorForBulbAtIndex:i];
      bulb.frame = CGRectMake(0.0f, 0.0f, bulb.diameter, bulb.diameter);
      if (i == 0) {
        bulb.center = CGPointMake(self.frame.size.width / 2.0f, _mainBulb.center.y - 160.0f);
      } else {
        bulb.center = CGPointMake(self.frame.size.width / 2.0f, _mainBulb.center.y + 160.0f);
      }
      bulb.originalCenter = bulb.center;
      bulb.delegate = self;
      bulb.parentView = self;
      [_bulbs addObject:bulb];
      [self addSubview:bulb];
    }
  } else {
    // TODO
  }
}

- (void)presentPillMenuItemsForPillBulb:(NMPillBulb *)pillBulb
                               animated:(BOOL)animated {
  if (animated) {
    CGFloat delay = 0.0f;

    for (NMPillMenuItem *i in _pillMenuItems) {
      if (i.isHidden) i.hidden = NO;
      [UIView animateWithDuration:0.2f
                            delay:delay
                          options:UIViewAnimationOptionCurveEaseInOut
                       animations:^(void) {
                         i.opaque = YES;
                         i.alpha = 1.0f;
                         // Check to which side the current bulb was bookmarked
                         switch (pillBulb.alignment) {
                           case kNMPillBulbAlignmentLeft:
                             i.frame = CGRectOffset(i.frame, +self.frame.size.width, 0.0f);
                             break;

                           case kNMPillBulbAlignmentRight: {
                             i.hidden = YES;
                             i.frame = CGRectOffset(i.frame, +self.frame.size.width * 2.0f, 0.0f);
                             i.hidden = NO;
                             i.frame = CGRectOffset(i.frame, -self.frame.size.width + 20.0f, 0.0f);
                             break;
                           }
                             
                           default:
                             break;
                         }
                       }
                       completion:^(BOOL finished) {
                         
                       }];
      delay += 0.2f;
    }
  }
}

- (void)dismissPillMenuItemsForPillBulb:(NMPillBulb *)pillBulb
                               animated:(BOOL)animated {
  if (animated) {
    CGFloat delay = 0.0f;

    for (NMPillMenuItem *i in _pillMenuItems) {
      i.opaque = NO;
      i.alpha = 0.4f;
      [UIView animateWithDuration:0.2f
                            delay:delay
                          options:UIViewAnimationOptionCurveEaseInOut
                       animations:^(void) {
                         switch (pillBulb.alignment) {
                           case kNMPillBulbAlignmentLeft:
                             i.frame = CGRectOffset(i.frame, -self.frame.size.width, 0.0f);
                             break;

                           case kNMPillBulbAlignmentRight: {
                             i.frame = CGRectOffset(i.frame, +self.frame.size.width - 20.0f, 0.0f);
                             i.hidden = YES;
                             break;
                           }
                             
                           default:
                             break;
                         }
                       }
                       completion:^(BOOL finished) {
                         
                       }];
      delay += 0.2f;
      switch (pillBulb.alignment) {
        case kNMPillBulbAlignmentRight:
          i.frame = CGRectOffset(i.frame, -self.frame.size.width * 2.0f, 0.0f);
          break;

        default:
          break;
      }
    }
  }
}

#pragma mark - NMPillBulb Delegate Section

- (void)pillViewBulb:(NMPillBulb *)bulb movedToNewOrigin:(CGPoint)newOrigin {
  if (newOrigin.x < self.frame.origin.x) {
    bulb.bookmarked = YES;
    [self.delegate willTransitPillViewBulb:bulb
                               atDirection:kNMTransitionDirectionLeft];
  } else if (newOrigin.x > self.frame.origin.x) {
    bulb.bookmarked = NO;
    [self rearrangePillsOriginalPositions];
  }
}

- (void)rearrangePillsOriginalPositions {
  [UIView beginAnimations:@"Rearrange" context:nil];
  [UIView setAnimationDuration:0.5f];
  [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];

  // Rearrange main bulb
  if (!_mainBulb.bookmarked) {
    _mainBulb.center = _mainBulb.originalCenter;
  }

  // Rearrange all other bulbs
  for (NMPillBulb *pb in _bulbs) {
    if (!pb.bookmarked) {
      pb.center = pb.originalCenter;
    }
  }

  [UIView commitAnimations];
  [_mainBulb startMovement];
}

@end
