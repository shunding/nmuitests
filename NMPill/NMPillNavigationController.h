//
//  NMNavigationViewController.h
//  NMUITests
//
//  Created by Alex Drozhak on 12/20/12.
//  Copyright (c) 2012 Alex Drozhak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class NMPillController;
@class NMPillNavigationBorder;
@class NMPillBulb;

@interface NMPillNavigationController : UIViewController

/*!
 * The top NMPillController object on the stack.
 */
@property (nonatomic, strong, readonly) NMPillController *topPillController;

/*!
 * Return the currently showed NMPillController.
 */
@property (nonatomic, strong, readonly) NMPillController *visiblePillController;

@property (nonatomic, strong) NMPillNavigationBorder *leftNavigationBorder;
@property (nonatomic, strong) NMPillNavigationBorder *topNavigationBorder;
@property (nonatomic, strong) NMPillNavigationBorder *rightNavigationBorder;
@property (nonatomic, strong) NMPillNavigationBorder *bottomNavigationBorder;

/*!
 * Convenience method pushes the root NMPillController without animation.
 * @param `NMPillController` main controller.
 * @result NMPillNavigationViewController object.
 */
- (id)initWithRoot:(NMPillController *)rootController;

- (void)showAllBordersAnimated:(BOOL)animated;

- (void)navigateToRightWithBulb:(NMPillBulb *)pillBulb;
- (void)navigateToLeftWithBulb:(NMPillBulb *)pillBulb;
- (void)navigateToTopWithBulb:(NMPillBulb *)pillBulb;

- (void)makeCurrentPillFullscreen:(NMPillBulb *)pillBulb;

@end
