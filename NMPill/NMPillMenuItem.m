//
//  NMPillMenuItem.m
//  NMUITests
//
//  Created by Alex Drozhak on 12/25/12.
//  Copyright (c) 2012 Alex Drozhak. All rights reserved.
//

#import "NMPillMenuItem.h"

@implementation NMPillMenuItem

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    self.backgroundColor = [UIColor clearColor];
    self.cornerRadius = 0.0f;
  }
  return self;
}

- (void)drawRect:(CGRect)rect {
  UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect
                                                  cornerRadius:self.cornerRadius];
  [_color setFill];
  [path fill];

  CGContextRef context = UIGraphicsGetCurrentContext();
  CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
  // Draw title
  CGRect titleFrame = rect;
  titleFrame.origin.y = titleFrame.size.height / 2.0f - 7.0f;
  titleFrame.origin.x += 30.0f;
  [_title drawInRect:titleFrame
            withFont:[UIFont boldSystemFontOfSize:14.0f]
       lineBreakMode:NSLineBreakByTruncatingMiddle
           alignment:NSTextAlignmentLeft];
}

@end
