//
//  NMPillController.m
//  NMUITests
//
//  Created by Alex Drozhak on 12/17/12.
//  Copyright (c) 2012 Alex Drozhak. All rights reserved.
//

#import "NMPillController.h"
#import "NMPillNavigationController.h"
#import "NMPillView.h"

@implementation NMPillController

#pragma mark - UIView Lifecycle Section

- (void)loadView {
  [super loadView];

  self.pillView = [[NMPillView alloc] initWithFrame:self.view.bounds];
  _pillView.delegate = self;
  _pillView.dataSource = self;

  _pillView.backgroundColor = [UIColor colorWithRed:206.0f / 255.0f
                                              green:206.0f / 255.0f
                                               blue:206.0f / 255.0f
                                              alpha:1.0f];
  [self.view addSubview:_pillView];
}

#pragma mark - NMPillView Delegate Section

- (void)didSelectPillViewBulb:(NMPillBulb *)pillViewBulb {
  NSLog(@"Selected Bulb with title %@", pillViewBulb.title);
  [self.pillNavigationController navigateToRightWithBulb:pillViewBulb];
}

- (void)willTransitPillViewBulb:(NMPillBulb *)pillViewBulb
                    atDirection:(NMTransitionDirection)direction {
  switch (direction) {
    case kNMTransitionDirectionLeft:
      [self.pillNavigationController navigateToRightWithBulb:pillViewBulb];
      break;

    case kNMTransitionDirectionRight:
      [self.pillNavigationController navigateToLeftWithBulb:pillViewBulb];
      break;

    case kNMTransitionDirectionBottom:
      [self.pillNavigationController navigateToTopWithBulb:pillViewBulb];

    default:
      break;
  }
}

- (void)didLongPressedPillViewBulb:(NMPillBulb *)pillViewBulb {
  [self.pillNavigationController makeCurrentPillFullscreen:pillViewBulb];
}

#pragma mark - NMPillView DataSource Section

- (NSArray *)pillMenuItemsForPillBulb:(NMPillBulb *)pillBulb {
  NSArray *menuItems = nil;
  if ([pillBulb.title isEqualToString:@"Nevermore"]) {
    menuItems = [NSArray arrayWithObjects:
                 @"Never...",
                 @"I will never...",
                 @"Do not...",
                 @"No more...",
                 @"Test...",
                 @"Another Test...", nil];
  }
  return menuItems;
}

- (NSUInteger)numberOfBulbsInPillView:(NMPillView *)pillView {
  return 2;
}

- (NMPillBulb *)bulbForPillViewAtIndex:(NSUInteger)bulbIndex {
  NMPillBulb *bulb = [[NMPillBulb alloc] initWithFrame:CGRectZero];
  switch (bulbIndex) {
    case 0:
      bulb.title = @"Feed";
      break;

    case 1:
      bulb.title = @"New";
      break;

    default:
      break;
  }

  return bulb;
}

- (NMPillBulb *)mainBulbForPillView {
  NMPillBulb *mainBulb = [[NMPillBulb alloc] initWithFrame:CGRectZero];
  mainBulb.color = [UIColor redColor];
  mainBulb.title = @"Nevermore";
  return mainBulb;
}

- (CGFloat)diameterForBulbAtIndex:(NSUInteger)bulbIndex {
  CGFloat diameter = 0.0f;
  switch (bulbIndex) {
    case 0:
      diameter = 100.0;
      break;

    case 1:
      diameter = 90.0f;
      break;

    default:
      break;
  }
  return diameter;
}

- (UIColor *)colorForBulbAtIndex:(NSUInteger)bulbIndex {
  switch (bulbIndex) {
    case 0:
      return [UIColor blueColor];

    case 1:
      return [UIColor greenColor];

    default:
      break;
  }
  return [UIColor redColor];
}


@end
