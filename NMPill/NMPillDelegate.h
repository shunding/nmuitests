//
//  NMPillDelegate.h
//  NMUITests
//
//  Created by Alex Drozhak on 12/17/12.
//  Copyright (c) 2012 Alex Drozhak. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
  kNMTransitionDirectionLeft,
  kNMTransitionDirectionRight,
  kNMTransitionDirectionBottom
} NMTransitionDirection;

@class NMPillBulb;

@protocol NMPillDelegate <NSObject>

- (void)didSelectPillViewBulb:(NMPillBulb *)pillViewBulb;
- (void)willTransitPillViewBulb:(NMPillBulb *)pillViewBulb
                    atDirection:(NMTransitionDirection)direction;

@optional
- (void)didLongPressedPillViewBulb:(NMPillBulb *)pillViewBulb;

@end
