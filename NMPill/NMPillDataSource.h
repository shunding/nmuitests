//
//  NMPillDataSource.h
//  NMUITests
//
//  Created by Alex Drozhak on 12/17/12.
//  Copyright (c) 2012 Alex Drozhak. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NMPillView;
@class NMPillBulb;

@protocol NMPillDataSource <NSObject>

- (NSUInteger)numberOfBulbsInPillView:(NMPillView *)pillView;
- (NMPillBulb *)bulbForPillViewAtIndex:(NSUInteger)bulbIndex;
- (CGFloat)diameterForBulbAtIndex:(NSUInteger)bulbIndex;
- (UIColor *)colorForBulbAtIndex:(NSUInteger)bulbIndex;
- (NMPillBulb *)mainBulbForPillView;

- (NSArray *)pillMenuItemsForPillBulb:(NMPillBulb *)pillBulb;

@end
