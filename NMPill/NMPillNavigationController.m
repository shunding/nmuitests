//
//  NMPillNavigationController.m
//  NMUITests
//
//  Created by Alex Drozhak on 12/20/12.
//  Copyright (c) 2012 Alex Drozhak. All rights reserved.
//

#import "NMPillNavigationController.h"
#import "NMPillNavigationBorder.h"
#import "NMPillController.h"
#import "NMPillBulb.h"
#import "NMPillView.h"

#define BORDER_WIDTH 20.0f

@interface NMPillNavigationController () {
  NMPillController *_root;

  NSMutableArray *_pillControllers;
}

@end

@implementation NMPillNavigationController

- (NMPillController *)visiblePillController {
  return [_pillControllers lastObject];
}

- (id)initWithRoot:(NMPillController *)rootController {
  self = [super init];
  if (self) {
    // Initialize local _root variable
    _root = rootController;
    _root.pillNavigationController = self;

    _pillControllers = [NSMutableArray array];
    [_pillControllers addObject:_root];
  }
  return self;
}

- (void)loadView {
  [super loadView];

  // Initialize and add all navigation borders
  CGRect borderFrame = CGRectMake(self.view.bounds.origin.x,
                                  self.view.bounds.origin.y,
                                  BORDER_WIDTH,
                                  self.view.bounds.size.height);
  self.leftNavigationBorder =
  [[NMPillNavigationBorder alloc] initWithFrame:borderFrame
                                        andType:kNMPillNavigationBorderTypeLeft];
  _leftNavigationBorder.borderColor = [UIColor lightGrayColor];
  [self.view addSubview:_leftNavigationBorder];
  [_leftNavigationBorder hideNavigationBorderAnimated:NO];

  borderFrame = CGRectMake(self.view.bounds.origin.x,
                           self.view.bounds.origin.y,
                           self.view.bounds.size.width,
                           BORDER_WIDTH);
  self.topNavigationBorder =
  [[NMPillNavigationBorder alloc] initWithFrame:borderFrame
                                        andType:kNMPillNavigationBorderTypeTop];
  _topNavigationBorder.borderColor = [UIColor greenColor];
  [self.view addSubview:_topNavigationBorder];
  [_topNavigationBorder hideNavigationBorderAnimated:NO];

  borderFrame = CGRectMake(self.view.bounds.size.width - BORDER_WIDTH,
                           self.view.bounds.origin.y,
                           BORDER_WIDTH,
                           self.view.bounds.size.height);
  self.rightNavigationBorder =
  [[NMPillNavigationBorder alloc] initWithFrame:borderFrame
                                        andType:kNMPillNavigationBorderTypeRight];
  _rightNavigationBorder.borderColor = [UIColor blueColor];
  [self.view addSubview:_rightNavigationBorder];
  [_rightNavigationBorder hideNavigationBorderAnimated:NO];

  borderFrame = CGRectMake(self.view.bounds.origin.x,
                           self.view.bounds.size.height - BORDER_WIDTH,
                           self.view.bounds.size.width,
                           BORDER_WIDTH);
  self.bottomNavigationBorder =
  [[NMPillNavigationBorder alloc] initWithFrame:borderFrame
                                        andType:kNMPillNavigationBorderTypeBottom];
  _bottomNavigationBorder.borderColor = [UIColor yellowColor];
  [self.view addSubview:_bottomNavigationBorder];
  [_bottomNavigationBorder hideNavigationBorderAnimated:NO];

  // Add root view controller's view

  CGRect rootViewFrame = self.view.bounds;
  _root.view.frame = rootViewFrame;
  [self.view addSubview:_root.view];
}

- (void)showAllBordersAnimated:(BOOL)animated {
  [_leftNavigationBorder showNavigationBorderAnimated:animated];
  [_topNavigationBorder showNavigationBorderAnimated:animated];
  [_rightNavigationBorder showNavigationBorderAnimated:animated];
  [_bottomNavigationBorder showNavigationBorderAnimated:animated];
}

#pragma mark - Navigation Section

- (void)makeCurrentPillFullscreen:(NMPillBulb *)pillBulb {
  [UIView beginAnimations:@"Bookmarked" context:nil];
  [UIView setAnimationDuration:0.5f];
  [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
  pillBulb.bookmarked = YES;
  pillBulb.frame = self.view.bounds;
  [UIView commitAnimations];
}

- (void)navigateToTopWithBulb:(NMPillBulb *)pillBulb {
  // Setup bulb's alignment property
  pillBulb.alignment = kNMPillBulbAlignmentBottom;
  [_bottomNavigationBorder showNavigationBorderAnimated:YES];
  [UIView beginAnimations:@"Bookmarked_1" context:nil];
  [UIView setAnimationDuration:0.5f];
  [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
  CGRect frame = pillBulb.frame;
  frame.origin.y += pillBulb.frame.size.height;
  pillBulb.frame = frame;
  [pillBulb setBookmarked:YES];
  pillBulb.frame = _bottomNavigationBorder.frame;
  [UIView commitAnimations];
}

- (void)navigateToLeftWithBulb:(NMPillBulb *)pillBulb {
  // Setup bulb's alignment property
  pillBulb.alignment = kNMPillBulbAlignmentRight;
  [_rightNavigationBorder showNavigationBorderAnimated:YES];
  [UIView beginAnimations:@"Bookmarked_1" context:nil];
  [UIView setAnimationDuration:0.5f];
  [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
  CGRect frame = pillBulb.frame;
  frame.origin.x += pillBulb.frame.size.width;
  pillBulb.frame = frame;
  [pillBulb setBookmarked:YES];
  pillBulb.frame = _rightNavigationBorder.frame;
  [UIView commitAnimations];

  [_root.pillView presentPillMenuItemsForPillBulb:pillBulb
                                         animated:YES];
}

- (void)navigateToRightWithBulb:(NMPillBulb *)pillBulb {
  // Setup bulb's alignment property
  pillBulb.alignment = kNMPillBulbAlignmentLeft;
  [_leftNavigationBorder showNavigationBorderAnimated:YES];
  
  [UIView beginAnimations:@"Bookmarked_1" context:nil];
  [UIView setAnimationDuration:0.5f];
  [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
  CGRect frame = pillBulb.frame;
  frame.origin.x -= pillBulb.frame.size.width;
  pillBulb.frame = frame;
  [pillBulb setBookmarked:YES];
  pillBulb.frame = _leftNavigationBorder.frame;
  [UIView commitAnimations];

  [_root.pillView presentPillMenuItemsForPillBulb:pillBulb
                                         animated:YES];
}

@end
