//
//  NMPillMenuItem.h
//  NMUITests
//
//  Created by Alex Drozhak on 12/25/12.
//  Copyright (c) 2012 Alex Drozhak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NMPillMenuItem : UIView

@property (nonatomic, strong) UIColor *color;
@property (nonatomic, strong) NSString *title;

// Corner radius (Default value equals 0.0f)
@property (nonatomic, assign) CGFloat cornerRadius;

@end
