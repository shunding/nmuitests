//
//  NMPillView.h
//  NMUITests
//
//  Created by Alex Drozhak on 12/17/12.
//  Copyright (c) 2012 Alex Drozhak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NMPillDelegate.h"
#import "NMPillDataSource.h"
#import "NMPillBulb.h"

@interface NMPillView : UIView <NMPillBulbDelegate>

@property (nonatomic, assign) id<NMPillDelegate> delegate;
@property (nonatomic, assign) id<NMPillDataSource> dataSource;

- (void)rearrangePillsOriginalPositions;

- (void)presentPillMenuItemsForPillBulb:(NMPillBulb *)pillBulb
                               animated:(BOOL)animated;
- (void)dismissPillMenuItemsForPillBulb:(NMPillBulb *)pillBulb
                               animated:(BOOL)animated;

@end
